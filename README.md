# Python开源扫雷游戏

### 简介

Python WxPython开源扫雷游戏PyMine新版1.5迁徙到Python 3.11和wxPython 4.2。并将所有图像放大一倍，以适合新的计算机的分辨率。并使用了没有边框的BitMapButton。

Python WxPython开源扫雷游戏PyMine新版1.4有少量API修改，并移植至Python 3.6和wxPython 4.0.3。本例为开源扫雷游戏PyMine 使用Python语言和WxPython UI框架 本例移植自本人开源例程JMine 请在程序所在目录使用python PyMine.py启动例程。需要先安装Python 3.6和wxPython 4.0.3框架

注意，本源码有两个分支，master分支上是Python 2.7写的1.2版本。py36_wx403分支上是新版1.4，是用Python 3.6和wxPython 4.0.3写的。Python3.11和wxPython 4.2的1.5 版本也在这个分支上。

截屏：

![输入图片说明](https://images.gitee.com/uploads/images/2018/1016/161439_4ce50801_1203742.png "pymine.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/1016/161450_4b3f81af_1203742.png "pymine_1.png")

#### 1.5版截图

![输入图片说明](PyMine15.png)

### 软件架构
Python 3.11, wxPython 4.2

### 安装
1. 装好Python 3.11
2. 装好wxPython 4.2
3. python PyMine.py运行（在Linux下可能为 python3 PyMine.py)
4. 可以使用Annaconda安装所需版本Python和wxPython