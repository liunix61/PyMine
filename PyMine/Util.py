import wx

def imgSize(pic,width,height):
    img = pic.ConvertToImage().Scale(width, height)   #将位图转换为图片后在改变尺寸
    bit = wx.Bitmap(img,wx.BITMAP_SCREEN_DEPTH)   #再次转换为位图
    return bit
